Gladepay Magento 2 Module 
======================

Magento2 extension for Gladepay payment gateway

Install
=======

* Go to Magento2 root folder

* Enter following command to install module:

```bash
composer require gpay/gladepay-magento2-module
```

* Wait while dependencies are updated.

* Enter following commands to enable module:

```bash
php bin/magento module:enable Gpay_Gladepay --clear-static-content
php bin/magento setup:upgrade
php bin/magento setup:di:compile
```


* Enable and configure `Gladepay` in *Magento Admin* under `Stores/Configuration/Payment` Methods

<!-- [ico-version]: https://img.shields.io/packagist/v/pstk/gladepay-magento2-module.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/pstk/gladepay-magento2-module.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/pstk/gladepay-magento2-module
[link-downloads]: https://packagist.org/packages/pstk/gladepay-magento2-module
 -->