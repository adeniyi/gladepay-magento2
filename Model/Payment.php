<?php

namespace Gpay\Gladepay\Model;

use Magento\Payment\Helper\Data as PaymentHelper;
use Gladepay\Gladepay;
use Exception;

class Payment implements \Gpay\Gladepay\Api\PaymentInterface
{
    const CODE = 'gpay_gladepay';

    protected $config;
    
    protected $gladepay;
    
    /**
     * @var EventManager
     */
    private $eventManager;

    public function __construct(
        PaymentHelper $paymentHelper,
        \Magento\Framework\Event\Manager $eventManager
    ) {
        $this->eventManager = $eventManager;
        $this->config = $paymentHelper->getMethodInstance(self::CODE);

        $merchantIdkey_Secret = $this->config->getConfigData('live_secret_key');
        $merchantId = $this->config->getConfigData('live_public_key');
        $base_url = "https://api.gladepay.com";
        
        if ($this->config->getConfigData('test_mode')) {
            $merchantIdkey_Secret = $this->config->getConfigData('test_secret_key');
            $merchantId = $this->config->getConfigData('test_public_key');
            $base_url = "https://demo.api.gladepay.com";
        }

        $this->gladepay = new Gladepay($merchantId, $merchantIdkey_Secret, $base_url);
    }

    /**
     * @return bool
     */
    public function verifyPayment($ref_quote)
    {
        // we are appending quoteid
        $ref = explode('_-~-_', $ref_quote);
        $reference = $ref[0];
        $quoteId = $ref[1];
        try {

            $transaction_details = $this->gladepay->transaction->verifyTransaction("$reference");

            if ($transaction_details->data->metadata->quoteId === $quoteId) {
                // dispatch the `payment_verify_after` event to update the order status
                $this->eventManager->dispatch('payment_verify_after');
                
                return json_encode($transaction_details);
            }
        } catch (Exception $e) {
            return json_encode([
                'status'=>0,
                'message'=>$e->getMessage()
            ]);
        }
        return json_encode([
            'status'=>0,
            'message'=>"quoteId doesn't match transaction"
        ]);
    }
}
