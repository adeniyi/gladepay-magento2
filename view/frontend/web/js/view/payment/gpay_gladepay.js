/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'gpay_gladepay',
                component: 'Gpay_Gladepay/js/view/payment/method-renderer/gpay_gladepay'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
