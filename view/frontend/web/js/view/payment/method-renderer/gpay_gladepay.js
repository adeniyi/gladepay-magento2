/*browser:true*/
/*global define*/
define([
  "jquery",
  "Magento_Checkout/js/view/payment/default",
  "Magento_Checkout/js/action/place-order",
  "Magento_Checkout/js/model/payment/additional-validators",
  "Magento_Checkout/js/model/quote",
  "Magento_Checkout/js/model/full-screen-loader",
  "Magento_Checkout/js/action/redirect-on-success"
], function(
  $,
  Component,
  placeOrderAction,
  additionalValidators,
  quote,
  fullScreenLoader,
  redirectOnSuccessAction
) {
  "use strict";

  return Component.extend({
    defaults: {
      template: "Gpay_Gladepay/payment/form",
      customObserverName: null
    },

    redirectAfterPlaceOrder: false,

    initialize: function() {
      this._super();

      var tempCheckoutConfig = window.checkoutConfig;
      var localGladepayConfiguration = tempCheckoutConfig.payment.gpay_gladepay;

      // Add Gladepay Gateway script to head
      if(localGladepayConfiguration.mode == "live"){
        $("head").append('<script type="text/javascript" src="http://api.gladepay.com/checkout.js"></script>');
      }else{
        $("head").append('<script type="text/javascript" src="http://demo.api.gladepay.com/checkout.js"></script>');
      }
      return this;
    },

    getCode: function() {
      return "gpay_gladepay";
    },

    getData: function() {
      return {
        method: this.item.method,
        additional_data: {}
      };
    },

    isActive: function() {
      return true;
    },

    /**
     * @override
     */
    afterPlaceOrder: function() {
      var checkoutConfig = window.checkoutConfig;
      var paymentData = quote.billingAddress();
      var gladepayConfiguration = checkoutConfig.payment.gpay_gladepay;

      if (checkoutConfig.isCustomerLoggedIn) {
        var customerData = checkoutConfig.customerData;
        paymentData.email = customerData.email;
      } else {
        var storageData = JSON.parse(
          localStorage.getItem("mage-cache-storage")
        )["checkout-data"];
        paymentData.email = storageData.validatedEmailValue;
      }

      var quoteId = checkoutConfig.quoteItemData[0].quote_id;

      var _this = this;
      _this.isPlaceOrderActionAllowed(false);

      initPayment({
          MID:"00",
          email: "hello@example.com",
          firstname:"John",
          lastname:"Doe",
          description: "",
          title: "",
          amount: 100,
          country: "NG",
          currency: "NGN",
          onclose: function() {

          },
          callback: function(response) {

          }
      });
         
      var handler = initPayment({
        MID: gladepayConfiguration.public_key,
        email: paymentData.email,
        amount: Math.ceil(quote.totals().grand_total * 100), // get order total from quote for an accurate... quote
        phone: paymentData.telephone,
        country: "NG",
        currency: "NGN",
        firstname:"",
        lastname:"",
        description: "",
        title: "",
        metadata: {
          quoteId: quoteId,
          custom_fields: [
            {
              display_name: "QuoteId",
              variable_name: "quote id",
              value: quoteId
            },
            {
              display_name: "Address",
              variable_name: "address",
              value: paymentData.street[0] + ", " + paymentData.street[1]
            },
            {
              display_name: "Postal Code",
              variable_name: "postal_code",
              value: paymentData.postcode
            },
            {
              display_name: "City",
              variable_name: "city",
              value: paymentData.city + ", " + paymentData.countryId
            }
          ]
        },
        callback: function(response) {
          $.ajax({
            method: "GET",
            url:
              gladepayConfiguration.api_url +
              "gladepay/verify/" +
              response.reference +
              "_-~-_" +
              quoteId
          }).success(function(data) {
            data = JSON.parse(data);

            if (data.status) {
              if (data.data.status === "success") {
                // redirect to success page after
                redirectOnSuccessAction.execute();
                return;
              }
            }

            _this.isPlaceOrderActionAllowed(true);
            _this.messageContainer.addErrorMessage({
              message: "Error, please try again"
            });
          });
        }
      });
      handler.openIframe();
    }
  });
});
